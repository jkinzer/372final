/**
 * 
 * @author Brahma Dathan and Sarnath Ramnath
 * @Copyright (c) 2010
 
 * Redistribution and use with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - the use is for academic purpose only
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Neither the name of Brahma Dathan or Sarnath Ramnath
 *     may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * The authors do not make any claims regarding the correctness of the code in this module
 * and are not responsible for any loss or damage resulting from its use.  
 */
import java.awt.*;
import java.awt.font.TextAttribute;
import java.util.Map;

/**
 * Implements a line; stores the end points.
 * 
 */
public class Label extends Item {
	private Point startingPoint;
	private String text = "";
	private Font fontNew;
	private boolean bold = false;

	/**
	 * Creates a label object with the starting point determined.
	 * 
	 * @param point
	 *            the start of the label
	 */
	public Label(Point point) {
		startingPoint = point;
	}

	/**
	 * Adds one more character to the label
	 * 
	 * @param character
	 *            a new character in the label
	 */
	public void addCharacter(char character) {
		text += character;
	}

	/**
	 * removes the rightmost character in the label
	 */
	public void removeCharacter() {
		if (text.length() > 0) {
			text = text.substring(0, text.length() - 1);
		}
	}

	/**
	 * Checks if the given point is in the label
	 */
	public boolean includes(Point point) {
		return distance(point, startingPoint) < 10.0;
	}

	/**
	 * Displays the label
	 */
	public void render() {
		uiContext.draw(this, fontNew);
	}

	/**
	 * Returns the actual text in the label
	 * 
	 * @return the text in the label
	 */
	public String getText() {
		return text;
	}

	/**
	 * Returns the starting point
	 * 
	 * @return starting point
	 */
	public Point getStartingPoint() {
		return startingPoint;

	}
	/**
	 * Sets the font
	 * @param font
	 */
	public void setFont(Font font) {
		fontNew = font;

	}
	/**
	 * gets the font
	 * @return
	 */
	public Font getFont() {
		return fontNew;

	}
	/**
	 * set text bold
	 */
	public void setBold() {

		String tempFont = fontNew.getFamily();
		int tempSize = fontNew.getSize();
		fontNew = new Font(tempFont, Font.BOLD, tempSize);

	}
	/**
	 * set text not bold
	 */
	public void unsetBold() {
		String tempFont = fontNew.getFamily();
		int tempSize = fontNew.getSize();
		fontNew = new Font(tempFont, Font.PLAIN, tempSize);

	}
	/**
	 * set text italic
	 */
	public void setItalic() {
		String tempFont = fontNew.getFamily();
		int tempSize = fontNew.getSize();
		fontNew = new Font(tempFont, Font.ITALIC, tempSize);

	}
	/**
	 * set text underlined
	 */
	public void setUnderline() {
		Font font = this.getFont();
		Map attributes = font.getAttributes();
		attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
		this.setFont(font.deriveFont(attributes));
	}

}