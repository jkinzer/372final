import java.awt.Point;

/**
 * incomplete rectangle class
 * @author Marshall, Jake
 *
 */
public class IncompleteRectangle extends Rectangle {
	/**
	 * Store the first point and create the object
	 * @param point
	 */
  public IncompleteRectangle(Point point) {
    super(point);
  }
  /**
   * Draw the line if NewSwingUI is being used.
   * Otherwise, nothing will be drawn.
   */
  public void render() {
    if (uiContext instanceof NewSwingUI) {
      ((NewSwingUI) uiContext).draw(this);
    }
  }
  /**
   * Nothing can be inside an incomplete line, so returns false
   */
  public boolean includes(Point point) {
    return false;
  }
}
