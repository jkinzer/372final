import java.awt.Font;
import java.awt.event.ActionEvent;

import javax.swing.JComboBox;
import javax.swing.JPanel;

/**
 * Class for the font size combo box
 * @author Marshall, Jake
 *
 */
public class FontSizeCombo extends JComboBox {
	  protected JPanel drawingPanel;
	  private LabelCommand labelCommand;
	  protected View view;
	  static String[] fontSizeArray = {"6", "7", "8","9","10","11","12","13","14","15" , "16", "17", "18", "19", "20", "21", "22","23","24","25","26","27","28","29", "30",
		  "31", "32", "33", "35", "36" , "37", "38", "39", "40"};
	   static Font font;
	   /**
	    * Constructor of font size combo
	    */
	  public FontSizeCombo()
	  {
		  super(fontSizeArray);
		  addActionListener(this);
		  font = new Font("Arial", Font.PLAIN, 12);
	  }
	  /**
	   * action performed when button clicked
	   */
	  public void actionPerformed(ActionEvent event)
	  {
		  JComboBox cb = (JComboBox)event.getSource();
		  String fontString = (String)cb.getSelectedItem();
		  FontStyleCombo.setSize(Integer.parseInt(fontString));
		 
	  }
	  
	
}
