/**
 * 
 * @author Brahma Dathan and Sarnath Ramnath
 * @Copyright (c) 2010
 
 * Redistribution and use with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - the use is for academic purpose only
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Neither the name of Brahma Dathan or Sarnath Ramnath
 *     may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * The authors do not make any claims regarding the correctness of the code in this module
 * and are not responsible for any loss or damage resulting from its use.  
 */
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ImageObserver;

/**
 * A UI that uses the swing package
 * 
 */
public class NewSwingUI implements UIContext, ImageObserver {
	private Graphics graphics;
	private static NewSwingUI swingUI;

	/**
	 * For the singleton pattern
	 */
	private NewSwingUI() {
	}

	/**
	 * Returns the instance
	 * 
	 * @return the instance
	 */
	public static NewSwingUI getInstance() {
		if (swingUI == null) {
			swingUI = new NewSwingUI();
		}
		return swingUI;
	}

	/**
	 * The Graphics object for drawing
	 * 
	 * @param graphics
	 *            the Graphics object
	 */
	public void setGraphics(Graphics graphics) {
		this.graphics = graphics;
	}

	/**
	 * Draws a label
	 * 
	 * @param label
	 *            the label
	 */
	public void draw(Label label, Font font) {
		if (label.getStartingPoint() != null) {
			if (label.getText() != null) {
				graphics.setFont(label.getFont());
				graphics.drawString(label.getText(), (int) label
						.getStartingPoint().getX(), (int) label
						.getStartingPoint().getY());
			}
		}
	}

	/**
	 * Draws an incomplete label
	 * 
	 * @param label
	 *            the label
	 */
	public void draw(IncompleteLabel label, Font font) {
		if (label.getStartingPoint() != null) {
			if (label.getText() != null) {
				graphics.setFont(font);
				graphics.drawString(label.getText(), (int) label
						.getStartingPoint().getX(), (int) label
						.getStartingPoint().getY());
			}
		}
		int length = graphics.getFontMetrics().stringWidth(label.getText());
		graphics.drawString("_",
				(int) label.getStartingPoint().getX() + length, (int) label
						.getStartingPoint().getY());
	}

	/**
	 * Draws a line
	 * 
	 * @param line
	 *            the line to be drawn
	 */
	public void draw(Line line) {
		graphics.drawLine(Math.round((float) (line.getPoint1().getX())),
				Math.round((float) (line.getPoint1().getY())),
				Math.round((float) (line.getPoint2().getX())),
				Math.round((float) (line.getPoint2().getY())));
	}

	/**
	 * Draws an incomplete line
	 * 
	 * @param line
	 *            the line to be drawn
	 */
	public void draw(IncompleteLine line) {
		if (line.getPoint1() != null) {
			if (line.getPoint2() != null) {
				graphics.drawLine(
						Math.round((float) (line.getPoint1().getX())),
						Math.round((float) (line.getPoint1().getY())),
						Math.round((float) (line.getPoint2().getX())),
						Math.round((float) (line.getPoint2().getY())));
			} else {
				graphics.fillRect(
						Math.round((float) (line.getPoint1().getX())),
						Math.round((float) (line.getPoint1().getY())), 5, 5);
			}
		}
	}

	/**
	 * Draws a rectangle
	 */
	public void draw(Rectangle rectangle) {
		graphics.drawLine(Math.round((float) (rectangle.getPoint1().getX())),
				Math.round((float) (rectangle.getPoint1().getY())),
				Math.round((float) (rectangle.getPoint2().getX())),
				Math.round((float) (rectangle.getPoint1().getY())));
		graphics.drawLine(Math.round((float) (rectangle.getPoint1().getX())),
				Math.round((float) (rectangle.getPoint1().getY())),
				Math.round((float) (rectangle.getPoint1().getX())),
				Math.round((float) (rectangle.getPoint2().getY())));
		graphics.drawLine(Math.round((float) (rectangle.getPoint1().getX())),
				Math.round((float) (rectangle.getPoint2().getY())),
				Math.round((float) (rectangle.getPoint2().getX())),
				Math.round((float) (rectangle.getPoint2().getY())));
		graphics.drawLine(Math.round((float) (rectangle.getPoint2().getX())),
				Math.round((float) (rectangle.getPoint2().getY())),
				Math.round((float) (rectangle.getPoint2().getX())),
				Math.round((float) (rectangle.getPoint1().getY())));
		graphics.drawImage(rectangle.getImage(), rectangle.getPoint1().x,
				rectangle.getPoint1().y,
				((rectangle.getPoint2().x - rectangle.getPoint1().x) + 1),
				((rectangle.getPoint2().y - rectangle.getPoint1().y) + 1), this);

	}

	/**
	 * draw an incomplete rectangle
	 * 
	 * @param rectangle
	 */
	public void draw(IncompleteRectangle rectangle) {
		if (rectangle.getPoint1() != null) {
			if (rectangle.getPoint2() != null) {
				graphics.drawLine(
						Math.round((float) (rectangle.getPoint1().getX())),
						Math.round((float) (rectangle.getPoint2().getX())),
						Math.round((float) (rectangle.getPoint1().getY())),
						Math.round((float) (rectangle.getPoint2().getY())));
			} else {
				graphics.fillRect(
						Math.round((float) (rectangle.getPoint1().getX())),
						Math.round((float) (rectangle.getPoint1().getY())), 5,
						5);
			}
		}
	}

	/**
	 * Captures undefined items
	 * 
	 * @param item
	 *            the item
	 */
	public void draw(Item item) {
		System.out.println("Cant draw unknown Item \n");
	}

	/**
	 * Mandatory class when drawing image
	 */
	@Override
	public boolean imageUpdate(Image arg0, int arg1, int arg2, int arg3,
			int arg4, int arg5) {
		// TODO Auto-generated method stub
		return false;
	}
}