import java.awt.Cursor;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.MouseInputAdapter;
import javax.swing.filechooser.FileNameExtensionFilter;
/**
 * Class to create and handle rectangle button actions
 * @author marsh_000
 *
 */
public class RectangleButton extends JButton implements ActionListener {
	protected JPanel drawingPanel;
	protected View view;
	private MouseHandler mouseHandler;
	private RectangleCommand rectangleCommand;

	/**
	 * Creates the button for the line
	 * 
	 * @param jFrame
	 *            the frame where the label is put
	 * @param jPanel
	 *            the panel within the frame
	 */
	public RectangleButton(View jFrame, JPanel jPanel) {
		super("Rectangle");
		addActionListener(this);
		view = jFrame;
		drawingPanel = jPanel;
		mouseHandler = new MouseHandler();
	}

	/**
	 * Handle click for creating a new line
	 */
	public void actionPerformed(ActionEvent event) {
		view.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
		drawingPanel.addMouseMotionListener((MouseMotionListener) mouseHandler);
		drawingPanel.addMouseListener((MouseListener) mouseHandler);

	}

	/**
	 * Handles mouse click so that the points can now be captured.
	 * 
	 */
	private class MouseHandler extends MouseInputAdapter {
		private int pointCount = 0;

		/**
		 * Handles mouse released
		 */
		@Override
		public void mouseReleased(MouseEvent event) {
			if (++pointCount == 1) {
				rectangleCommand = new RectangleCommand(View.mapPoint(event
						.getPoint()));
				UndoManager.instance().beginCommand(rectangleCommand);
			} else if (pointCount == 2) {
				Image image = null;
				pointCount = 0;
				rectangleCommand.setLinePoint(View.mapPoint(event.getPoint()));
				int choice = JOptionPane.showConfirmDialog(drawingPanel,
						"Would you like to add a photo?");
				if (choice == JOptionPane.YES_OPTION) {
					JFileChooser chooser = new JFileChooser();
					FileNameExtensionFilter filter = new FileNameExtensionFilter(
							"JPG & GIF Images", "jpg", "gif");
					chooser.setFileFilter(filter);
					int returnVal = chooser.showOpenDialog(drawingPanel);
					if (returnVal == JFileChooser.APPROVE_OPTION) {
						System.out.println("You chose to open this file: "
								+ chooser.getSelectedFile().getName());
					}
					// String name=
					// JOptionPane.showInputDialog("Enter File Name of Picture to Load: ");
					try {
						File image2 = new File(chooser.getSelectedFile()
								.getAbsolutePath());
						image = ImageIO.read(image2);
						rectangleCommand.setImage(image);

					} catch (IOException e) {
						System.out.println("no image found");
					}
				}
				drawingPanel.removeMouseListener(this);
				view.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
				UndoManager.instance().endCommand(rectangleCommand);

			}
		}

	}
}
