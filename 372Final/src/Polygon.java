import java.awt.Point;
import java.util.Iterator;
import java.util.LinkedList;
/**
 * Class for drawing a polygon
 * @author Marshall and Jake
 *
 */
public class Polygon extends Item {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//for usage with making lines
	private LinkedList<Point> points = new LinkedList<Point>();
	private LinkedList<Line> lines = new LinkedList<Line>();
	/**
	 * Class to add line to the list
	 * @param line
	 */
	public void addLine(Line line) {
		lines.add(line);
	}
	/**
	 * Makes the lines based off of stored points
	 */
	private void makeLines() {
		for (int i = 0; i < points.size(); i++) {
			if ((i + 1) < points.size()) {
				addLine(new Line(points.get(i), points.get(i + 1)));
			} else {
				//attach last point to first point, closing polygon
				addLine(new Line(points.get(i), points.getFirst()));
			}
		}
	}
	/**
	 * Makes the lines, then render from the saved list
	 */
	public void render() {
		makeLines();
		for(Line line : lines){
			line.render();
		}
	}
	/**
	 * Checks if point included in any of the lines in the polygon
	 */
	@Override
	public boolean includes(Point point) {
		//checks if any of the lines include the point
		for(Line line : lines){
			if(line.includes(point)){
				return true;
			}
		}
		//will reach if none are included
		return false;
	}
	/**
	 * adds a point to the list for line making
	 * @param point
	 */
	public void addPoint(Point point) {
		points.add(point);
	}
}
