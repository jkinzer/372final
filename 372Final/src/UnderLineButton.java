import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;


public class UnderLineButton extends JButton implements ActionListener {
	private UnderlineCommand underlineCommand;
	/**
	 * Creates the button with the proper text and
	 * makes itself listen to clicks.
	 */
	public UnderLineButton() {
		super("Underline");
		addActionListener(this);
	}
	/**
	 * Processes the click by calling the deleteItems method
	 * of controller.
	 */
	public void actionPerformed(ActionEvent event) {
		underlineCommand = new UnderlineCommand();
	    UndoManager.instance().beginCommand(underlineCommand);
	    UndoManager.instance().endCommand(underlineCommand);
	}
}
