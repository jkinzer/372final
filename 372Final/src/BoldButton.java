import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

/**
 * Class for the bold button
 * @author Marshall, Jake
 *
 */
public class BoldButton extends JButton implements ActionListener {
	private BoldCommand boldCommand;
	/**
	 * Creates the button with the proper text and
	 * makes itself listen to clicks.
	 */
	public BoldButton() {
		super("Bold");
		addActionListener(this);
	}
	/**
	 * Processes the click by calling the deleteItems method
	 * of controller.
	 */
	public void actionPerformed(ActionEvent event) {
		boldCommand = new BoldCommand();
	    UndoManager.instance().beginCommand(boldCommand);
	    UndoManager.instance().endCommand(boldCommand);
	}
}
