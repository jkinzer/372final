import java.util.Enumeration;
import java.util.Vector;

/**
 * Class for bold button commands
 * @author Marshall, Jake
 *
 */
public class BoldCommand extends Command {
	  private Vector itemList;
	  /**
	   * Asks the model for the list of all selected items,
	   * so the deletions can be undone if needed.
	   */
	  public BoldCommand () {
	    itemList = new Vector();
	    Enumeration enumeration = model.getSelectedItems();
	    while (enumeration.hasMoreElements()) {
	      Item item = (Item)(enumeration.nextElement());
	      itemList.add(item);
	    }
	    model.boldSelectedItems();
	  }
	  /**
	   * Undoes the operation by adding the items to the selected
	   * list in the model.
	   */
	  public boolean undo() {
	    Enumeration enumeration = itemList.elements();
	    while (enumeration.hasMoreElements()) {
	      Item item = (Item) (enumeration.nextElement());
	      model.undoBold(item);
	      
	    }
	    return true;
	  }
	  /**
	   * Redoes the command  by re-executing it.
	   */
	  public boolean redo() {
	    execute();
	    return true;
	  }
	  /**
	   * Re-executes the command.
	   */
	  public void execute() {
	    model.boldSelectedItems();
	  }
	  
	  
}
