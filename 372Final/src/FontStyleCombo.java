import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JPanel;

/**
 * Font style class
 * @author Marshall, Jake
 *
 */
public class FontStyleCombo extends JComboBox implements ActionListener {
	  protected JPanel drawingPanel;
	  private LabelCommand labelCommand;
	  protected View view;
	  static String[] fontArray = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
	  static Font font;
	  /**
	   * Font style constructor
	   */
	  public FontStyleCombo()
	  {
		  super(fontArray);
		  addActionListener(this);
		  font = new Font("Arial", Font.PLAIN, 12);
	  }
	  /**
	   * action performed when button clicked
	   */
	  public void actionPerformed(ActionEvent event)
	  {
		  JComboBox cb = (JComboBox)event.getSource();
		  String fontString = (String)cb.getSelectedItem();
		  int tempSize = font.getSize();
		  int temp = font.BOLD;
		  font =  new Font(fontString, temp, tempSize);
		 
	  }
	  /**
	   * Set the font size
	   * @param size
	   */
	  public static void setSize(int size)
	  {
		 String temp = font.getFamily();
		 font = new Font(temp, Font.PLAIN, size);
	  }
	  /**
	   * Set whether bold or not
	   * @param bold
	   */
	  public static void setBold(boolean bold)
	  {
		  if(bold == true)
		  {
			  String tempFont = font.getFamily();
			  int tempSize = font.getSize();
			  font = new Font(tempFont, Font.BOLD, tempSize);
		  }
		  else
		  {
			  String tempFont = font.getFamily();
			  int tempSize = font.getSize();
			  font = new Font(tempFont, Font.PLAIN, tempSize);
		  }
	  }
}
