import java.awt.*;
/**
 * This represents a line that is being drawn. It will be drawn
 * by the view as it sees fit.
 *
 */
class IncompleteLine extends Line {
	/**
	 * Store the first point and create the object
	 * @param point
	 */
  public IncompleteLine(Point point) {
    super(point);
  }
  /**
   * Draw the line if NewSwingUI is being used.
   * Otherwise, nothing will be drawn.
   */
  public void render() {
    if (uiContext instanceof NewSwingUI) {
      ((NewSwingUI) uiContext).draw(this);
    }
  }
  /**
   * Nothing can be inside an incomplete line, so returns false
   */
  public boolean includes(Point point) {
    return false;
  }
}