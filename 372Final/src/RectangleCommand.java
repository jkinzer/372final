import java.awt.Image;
import java.awt.Point;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
/**
 * rectangle command class
 * @author Marshall, Jake
 *
 */
public class RectangleCommand extends Command {
	private Rectangle rectangle;
	private int pointCount;

	/**
	 * Creates a line command with one end point.
	 * 
	 * @param point
	 *            one of the end points
	 */
	public RectangleCommand(Point point) {
		rectangle = new IncompleteRectangle(point);
	}
	/**
	 * sets the image
	 * @param icon
	 */
	public void setImage(Image icon) {
		 rectangle.setImage(icon);
	}

	/**
	 * Sets the second end point
	 * 
	 * @param point
	 *            the second point
	 */
	public void setLinePoint(Point point) {
		rectangle.setPoint2(point);
	}

	/**
	 * Executes the command to add the item to the model
	 */
	public void execute() {
		model.addItem(rectangle);
	}

	/**
	 * Undoes the command by removing the item from the model
	 */
	public boolean undo() {
		model.removeItem(rectangle);
		return true;
	}

	/**
	 * Brings the line back by calling execute
	 */
	public boolean redo() {
		execute();
		return true;
	}

	/**
	 * Ends the command by setting the second end point the same as the first,
	 * if needed
	 */
	public void end() {
		model.removeItem(rectangle);
		rectangle = new Rectangle(rectangle.getPoint1(), rectangle.getPoint2(),rectangle.getImage());
		execute();
	}
}
