import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

/**
 * italic styling option button
 * @author Marshall Jake
 *
 */
public class ItalicButton extends JButton implements ActionListener{
	private ItalicCommand italicCommand;
	/**
	 * Creates the button with the proper text and
	 * makes itself listen to clicks.
	 */
	public ItalicButton() {
		super("Italic");
		addActionListener(this);
	}
	/**
	 * Processes the click by calling the deleteItems method
	 * of controller.
	 */
	public void actionPerformed(ActionEvent event) {
		italicCommand = new ItalicCommand();
	    UndoManager.instance().beginCommand(italicCommand);
	    UndoManager.instance().endCommand(italicCommand);
	}
}
