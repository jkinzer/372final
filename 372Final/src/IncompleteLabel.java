import java.awt.*;
/**
 * Creates a label object that is still being created by the user.
 *
 */
public class IncompleteLabel extends Label {
	 private Font fontNew;
  /**
	 * Creates the label with the starting point
	 * @param point the starting point
  */
  public IncompleteLabel(Point point) {
    super(point);
  }
  /**
   * Draw the line if NewSwingUI is being used.
   * Otherwise, nothing will be drawn.
   */
  public void render() {
    if (uiContext instanceof NewSwingUI) {
      ((NewSwingUI) uiContext).draw(this, fontNew);
    }
  }
  /**
   * Nothing can be inside an incomplete line, so returns false
   */
  public boolean includes(Point point) {
    return false;
  }
  /**
   * Set the font
   */
  public void setFont(Font font) {
		fontNew = font;
		
	}

}